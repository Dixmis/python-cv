from flask import Flask, abort, redirect
from flask import render_template
import yaml

app = Flask(__name__)


@app.route("/reload-config")
def reload_config():
    global projects_list
    global projects_list_flatten
    global projects_categories
    projects_categories = yaml.load(open("config/project_categories.yaml", encoding="utf-8"), Loader=yaml.FullLoader)
    projects_list = {}
    projects_list_flatten = {}
    project_list = []
    for categories in projects_categories["categorie_name"]:
        categorie = list(categories.keys())[0]
        projects = yaml.load(open(f"config/project_list_{categorie}.yaml", encoding="utf-8"), Loader=yaml.FullLoader)
        projects_list[categorie] = projects
        project_list += projects["projects"].copy()
        projects_list_flatten |= projects.copy()
    projects_list_flatten['projects'] = project_list
    return redirect("/")


projects_list = {}
projects_list_flatten = {}
projects_categories = {}
reload_config()


@app.route("/")
def homepage():
    return render_template("index.jinja2")


@app.route("/projects")
@app.route("/projects/")
def project_all_listpage():
    return render_template("project_list.jinja2", project_info=projects_list_flatten, categorie="*")


@app.route("/projects/<string:categorie>")
def project_listpage(categorie):
    if categorie == "*" or categorie == "" or categorie == "projects":
        categorie = "*"
        return render_template("project_list.jinja2", project_info=projects_list_flatten, categorie=categorie,
                               projects_categories=projects_categories)
    elif categorie in projects_list:
        print(projects_list[categorie])
        return render_template("project_list.jinja2", project_info=projects_list[categorie], categorie=categorie,
                               projects_categories=projects_categories)
    else:
        return abort(404)


@app.route("/project/<string:name>")
def show_project(name):
    if name == "projects":
        return abort(403)
    if name not in projects_list_flatten['projects']:
        return abort(403)
    if name in projects_list_flatten.keys():
        # return render_template("project_detail.html", project=projects_list_univ[name])
        return redirect(f"https://{projects_list_flatten[name]['url']}", 301)
    return abort(404)


@app.route("/var")
def show_vars():
    print(projects_categories)
    print(projects_list)
    print(projects_list_flatten)
    return redirect("/")
